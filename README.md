## Quistonnaire

### What it's for

Quistonnaire has been created to make coffee table conversation topic changes easier! You can save your questions or topics for later and with only one click you can get a new topic!

## Manual

* npm i
* npm start
