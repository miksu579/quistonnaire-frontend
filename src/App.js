import React, { Component } from "react";
import axios from "axios";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Main from "./components/Main/Main";
import QuestionOverview from "./components/QuestionOverview/QuestionOverview";
import NewQuestion from "./components/NewQuestion/NewQuestion";
import Navigation from "./components/Navigation/Navigation";
import EditQuestion from "./components/EditQuestion/EditQuestion";

const url = "https://generic-api-endpoint.com/quistonnaire";

class App extends Component {
  state = {
    questions: [],
    notAskedQuestions: [],
    askedQuestions: [],
    detailedQuestions: [],
    currentQuestion: { id: 0, name: "" },
    selectedQuestion: null,
  };
  async setQuestionsToState() {
    console.log("This comes first from setQuestionsToState");
    const response = await axios.get(`${url}/questions`);
    console.log("Response: " + response);
    let detailedQuestionArray = [];
    response.data.map((item) => {
      detailedQuestionArray.push(item);
    });

    this.setState({ questions: detailedQuestionArray });
    this.setState({ detailedQuestions: detailedQuestionArray });
    this.setState({ notAskedQuestions: detailedQuestionArray });
  }

  async componentDidMount() {
    await this.setQuestionsToState();
    this.changeQuestion();
    console.log("question changed");
  }

  saveNewQuestion = (questionName) => {
    console.log("this goes through first");
    if(questionName === "" || !questionName){
      alert("please fill in a question with the allowed characters 1-z,0-9,'!','?','.',','.");
      return;
    }

    axios
      .post(`${url}/newquestion`, { name: questionName })
      .then(this.setQuestionsToState());
    console.log("this goes through last");
  };

  changeSelectedQuestion = (event) => {
    this.setState({
      selectedQuestion: { name: event.target.innerHTML, id: event.target.id },
    });
  };

  changeQuestion = () => {
    //check if there are questions left in notAskedQuestions Array
    if (this.state.notAskedQuestions.length === 0) {
      let copyCurrentQuestion = this.state.currentQuestion;
      copyCurrentQuestion.name = "Kaikki kysymykset kysytty!";
      copyCurrentQuestion.id = 0;
      this.setState({ currentQuestion: copyCurrentQuestion });
      return;
    }
    //choose random index from notAskedQuestions
    let currentQuestionIndex = Math.floor(
      Math.random() * Math.floor(this.state.notAskedQuestions.length)
    );
    //sets randomly chosen question to state as currentQuestion
    this.setState({
      currentQuestion: this.state.notAskedQuestions[currentQuestionIndex],
    });
    //add currentQuestion to askedQuestions
    let copyAsked = [...this.state.askedQuestions];
    copyAsked.push(this.state.notAskedQuestions[currentQuestionIndex]);
    this.setState({ askedQuestions: copyAsked });
    //splice current question from notAskedQuestions
    let copyNotAsked = [...this.state.notAskedQuestions];
    copyNotAsked.splice(currentQuestionIndex, 1);
    this.setState({ notAskedQuestions: copyNotAsked });
  };

  //Used by EditQuestion component
  updateQuestionList = (obj) => {
    let copyDetailedQuestions = [...this.state.detailedQuestions];
    //Goes through detailedQuestions from state to change the specific questions name, then returns.
    if (this.state.currentQuestion.name === obj.name) {
      let copyCurrentQuestion = this.state.currentQuestion;
      copyCurrentQuestion.name = obj.name;
      this.setState({ currentQuestion: copyCurrentQuestion });
    }
    for (let i = 0; i < copyDetailedQuestions.length; i++) {
      if (parseInt(copyDetailedQuestions[i].id) === parseInt(obj.id)) {
        console.log(
          `found you ${copyDetailedQuestions[i].name} from now on you are ${obj.name}`
        );
        copyDetailedQuestions[i].name = obj.name;
        this.setState({ detailedQuestions: copyDetailedQuestions });
        return;
      }
    }
  };

  deleteQuestionFromState = () =>{
    if (this.state.selectedQuestion.id === this.state.currentQuestion.id){
      this.changeQuestion();
    }

    let copyDetailedQuestions = [...this.state.detailedQuestions];
    for (let i = 0; i < copyDetailedQuestions.length; i++) {
      if (
        parseInt(
          copyDetailedQuestions[i].id) ===
            parseInt(this.state.selectedQuestion.id)
      ) {
        copyDetailedQuestions.splice(i, 1);
        this.setState({ detailedQuestions: copyDetailedQuestions });
        return;
      }
    }
  };

  render() {
    return (
      <Router>
        <Navigation></Navigation>
        <Switch>
          <Route path="/quistonnaire" component={Main} exact>
            <Main
              changeQuestion={this.changeQuestion}
              currentQuestion={this.state.currentQuestion.name}
            ></Main>
          </Route>
          <Route path="/quistonnaire/questionoverview" component={QuestionOverview}>
            <QuestionOverview
              selectedQuestion={this.state.selectedQuestion}
              detailedQuestions={this.state.detailedQuestions}
              changeSelectedQuestion={this.changeSelectedQuestion}
            ></QuestionOverview>
          </Route>
          <Route path="/quistonnaire/newquestion" component={NewQuestion}>
            <NewQuestion saveNewQuestion={this.saveNewQuestion}></NewQuestion>
          </Route>
          <Route path="/quistonnaire/editquestion" component={EditQuestion}>
            <EditQuestion
              selectedQuestion={this.state.selectedQuestion}
              deleteQuestionFromState={this.deleteQuestionFromState}
              updateQuestionList={this.updateQuestionList}
            ></EditQuestion>
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default App;
