import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route,Link } from "react-router-dom";
import Axios from "axios";
import './editquestion.css';

const url = "https://generic-api-endpoint.com/quistonnaire";

class EditQuestion extends Component {
  state = {
    questionName: this.props.selectedQuestion ? this.props.selectedQuestion.name : null,
    questionId: this.props.selectedQuestion ? this.props.selectedQuestion.id : null,
  };

  onInputChange = (e) => {
    this.setState({ questionName: e.target.value });
    this.testQuestionName(e);
  };

  saveQuestion = () => {
    console.log(this.state.questionId, this.state.questionName);
    if(this.state.questionName === "" || !this.state.questionName){
      alert("please fill in a question with the allowed characters 1-z,0-9,'!','?','.',','.");
      return;
    }
    Axios.post(`${url}/savequestion`, {
      name: this.state.questionName,
      id: this.state.questionId,
    }).then(console.log("DONE"));
    //Make this update the questions to state, app can reload questions on refresh anyways.
    this.props.updateQuestionList({
      id: this.state.questionId,
      name: this.state.questionName,
    });
  };

  testQuestionName = (event) =>{
    let re = /^[\sA-Za-z0-9!?,.äö]+$/g;
    let link = document.getElementsByClassName("link-to-save-edit")[0];
    let button = document.getElementsByClassName("save-edit")[0];

    if(!re.exec(event.target.value)){
      console.log("pass");
      button.setAttribute('disabled', "true");
      button.setAttribute('style', "background-color: #e6c88f9e");
      link.setAttribute('style', "pointer-events:none");
      return;
    }
      button.setAttribute('style', "background-color: #e6c88f");
      link.setAttribute('style', "pointer-events:all");
    button.setAttribute('disabled', "false");
    
  }

  removeQuestion = () => {
    if (!this.props.selectedQuestion){
      alert("No selected question!!");
      return;
    } 
    Axios.delete(`${url}/deletequestion`, {data: this.props.selectedQuestion}).then(
      this.props.deleteQuestionFromState()
    );
  };

  render() {
    return (
      <div className="editquestion-container">
          <textarea
          className="question-textarea"
            placeholder="Your question, please use basic alphabets (ä and ö are allowed), numbers and ,.!? from special characters."
            value={this.state.questionName}
            onChange={this.onInputChange}
          ></textarea>
          <div className="button-container">
        <Link className="link-to-save-edit" to="/quistonnaire">
          <button className="save save-edit" onClick={this.saveQuestion}>
            SAVE
          </button>
        </Link>
        <Link to="/quistonnaire/questionoverview">
          <button className="remove" onClick={this.removeQuestion}>
            REMOVE
          </button>
        </Link>
        </div>
      </div>
    );
  }
}
export default EditQuestion;
