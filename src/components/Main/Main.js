import React, { Component } from "react";

class Main extends Component {
  render() {
    return (
      <div className="main-container">
        <div id="question">{this.props.currentQuestion}</div>
        <button id="changeQuestion-btn" onClick={this.props.changeQuestion}>
          Vaihda kysymystä
        </button>
      </div>
    );
  }
}
export default Main;
