import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./navigation.css"
import home from '../../images/home.svg';
import add from '../../images/add.svg';
import list from '../../images/list.svg';


class Navigation extends Component {
  render() {
    return (
      <div className="navigation-container">
        <ul>
          <Link to="/quistonnaire">
            <li>Home</li>
            <img className="icon" src={home}></img>
          </Link>
          <Link to="/quistonnaire/newquestion">
            <li>Add question</li>
            <img className="icon" src={add}></img>
          </Link>
          <Link to="/quistonnaire/questionoverview">
            <li>Question overview</li>
            <img className="icon" src={list}></img>
          </Link>
        </ul>
      </div>
    );
  }
}
export default Navigation;
