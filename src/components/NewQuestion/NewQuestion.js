import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import './newquestion.css';

class NewQuestion extends Component {
  state= {
    questionName: ""
  }

  onInputChange = (event) =>{
    this.setState({questionName: event.target.value});
    this.testQuestionName(event);
  }

  testQuestionName = (event) =>{
    let re = /^[\sA-Za-z0-9!?,.äö]+$/g;
    let link = document.getElementsByClassName("link-to-save")[0];
    let button = document.getElementsByClassName("save-btn")[0];
    
    if(!re.test(event.target.value)){
      button.setAttribute('disabled', "true");
      button.setAttribute('style', "background-color: #e6c88f9e; cursor: default;");
      link.setAttribute('style', "pointer-events:none");
      return;
    }
      button.setAttribute('style', "background-color: #e6c88f; cursor: pointer;");
      link.setAttribute('style', "pointer-events:all");
    button.setAttribute('disabled', "false");
    
  }

  saveNewQuestion = () =>{
    this.props.saveNewQuestion(this.state.questionName);
  }
  
  render() {
    return (
      <div className="newquestion-container">
        <textarea placeholder="Your question, please use basic alphabets (ä and ö are allowed), numbers and ,.!? from special characters." onChange={this.onInputChange} value={this.state.questionName} maxLength='255'></textarea>
        <Link className="link-to-save" to="/quistonnaire">
          <button className="save-btn" onClick={this.saveNewQuestion}>SAVE</button>
        </Link>
      </div>
    );
  }
}
export default NewQuestion;
