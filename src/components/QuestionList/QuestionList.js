import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import './questionlist.css';

class QuestionList extends Component {
  render() {
    return (
      <div className="questionlist-container">

        {this.props.detailedQuestions.map((item) => {
          return (
            <Link to="/quistonnaire/editquestion">
              <div className="question-container">
                <div
                  className="name"
                  id={item.id}
                  onClick={this.props.changeSelectedQuestion}
                >
                  {item.name}
                </div>
              </div>
            </Link>
          );
        })}
      </div>
    );
  }
}
export default QuestionList;
