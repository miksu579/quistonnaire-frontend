import React, { Component } from "react";
import QuestionList from "../QuestionList/QuestionList";
import './questionoverview.css';

class QuestionOverview extends Component {
  render() {
    return (
      <div className="questionOverview-container">
        <QuestionList
          detailedQuestions={this.props.detailedQuestions}
          changeSelectedQuestion={this.props.changeSelectedQuestion}
          selectedQuestion={this.props.selectedQuestion}
        ></QuestionList>
      </div>
    );
  }
}
export default QuestionOverview;
